/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringtask4;
import java.util.Scanner;
/**
 *
 * @author Zver
 */
public class StringTask4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String text;
        text = input.nextLine();
        long count = text.codePoints().filter(Character::isDigit).count();
        System.out.println(count);
                
    }
    
}
