/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cycle.pkg7;
import java.util.Scanner;
/**
 *
 * @author Zver
 */
public class Cycle7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double N;
        N = input.nextInt();
        double sum = 1;
        for( double i = 1; i<N; i++){
            sum = sum + 1/i;
        }
        System.out.println("sum = "+sum);
    }
    
}
