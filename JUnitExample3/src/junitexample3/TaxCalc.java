/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junitexample3;

/**
 *
 * @author Zver
 */
public class TaxCalc {
    public static Tax calcTax(double salary){
        Tax tax = new Tax();
        tax.pension = 0.22 * salary;
        tax.pn = 0.18 * salary;
        tax.prof = 0.04 * salary;
        return tax;
    }
    
}
