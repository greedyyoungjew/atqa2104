/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cycle12;
import java.util.Scanner;
/**
 *
 * @author Zver
 */
public class Cycle12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N;
        N = input.nextInt();
        double d = 1;
        for(int i = 1; i<=N; i++){
            d = d*i;
            System.out.println(i);
        }
        System.out.println(" N! = "+d);
    }
    
}
