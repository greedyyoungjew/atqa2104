/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myobjects;

/**
 *
 * @author Zver
 */
public class Cleaner implements IEmployee  {
    private double base;
    public Cleaner(double base)
    {
        this.base = base;
    }
    
    @Override
    public double calcSalary() {
        return this.base;
    }

    @Override
    public String getTitle() {
        return "Cleaner";
    }

     @Override
    public double calcTax() {
        return this.calcSalary() * 0.065;//6.5%
    }
    
}
