/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myobjects;

/**
 *
 * @author Zver
 */
public interface IEmployee {
    double calcSalary();
    String getTitle();
    double calcTax();
    
}
