
package stringtest.pkg1;


public class StringTest1 {

    
    public static void main(String[] args) {
        String inputString = "JAVA core";
        String reversed = new StringBuffer(inputString).reverse().toString();
        System.out.println(""+inputString+" = "+reversed);
    }
    
}
