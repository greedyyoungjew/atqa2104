/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavengdbstest;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
public class mavengdbstest {


    public static void main(String[] args) {
        
       try {
            System.out.println("Hello from Maven Project");
            //0. +Подключаю драйвер
            //1. Регистрирую класс драйвера в проекте
            Class.forName("org.sqlite.JDBC");
            //2. Создаю и настраиваю соединение с базой данных
            Connection conn = null;
            conn = DriverManager.
            getConnection("jdbc:sqlite:C:\\MyData\\test.db");
             java.sql.Statement stm = conn.createStatement();
             String text = "SELECT NAME, POSITIONS, SALARY FROM EMPLOYEES";
             ResultSet rs = stm.executeQuery(text);
                     while (rs.next())
             {
                float id = rs.getFloat("SALARY");
                String str1 = rs.getString("NAME");
                String str2 = rs.getString("POSITIONS");
                System.out.println("имя " + str1);
                System.out.println("должность " + str2);
                System.out.println("заработная плата "+id);
             }
            conn.close();  
             
        } catch (ClassNotFoundException ex) {
            System.out.println("Не тот файл драйвера используем");
        } catch (SQLException ex) {
            System.out.println("Не найден файл базы данных");
        }
    }
    
}
