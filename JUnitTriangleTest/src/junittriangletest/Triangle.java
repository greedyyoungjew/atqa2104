/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junittriangletest;

/**
 *
 * @author Zver
 */
public class Triangle {
    private double a;
    private double b;
    private double c;
    private double p;

    public double getP() {
                this.p = (this.getA()+this.getB()+this.getC())/2;

        return p;
    }

    public void setP(double p) {
        this.p = p;
    }
    public double calcArea(){return Math.sqrt(this.getP()*(this.getP()-this.getA())*(this.getP()-this.getB())*(this.getP()-this.getC()));}
    public double calcP(){return this.getA()+this.getB()+this.getC();}

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
}
