
package animalex;


public class MyPet extends Animal {

    @Override
    public String toString() {
        
        String result = this.nickname 
                + ", sex = "+this.getSex()
                + ", age (months) = "+ this.getAge();
        
        return result;
    }
    private String nickname;
    MyPet(String nick, int sex, int age)
    {
        super(sex, age);//передал данные на уровень продителя
        //вызвал его конструктор
        this.nickname = nick;
    }
}
