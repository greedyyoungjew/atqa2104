/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalex;

public class Animal {

    public int getSex() {
        return sex;
    }
    private int sex;
    private int age;//в месяцах

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal{" + "sex=" + sex + ", age=" + age + '}';
    }

    public Animal(int sex, int age) {
        this.sex = sex;
        this.age = age;
    }
    
}
