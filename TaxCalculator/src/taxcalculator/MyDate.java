
package taxcalculator;


public class MyDate {
    private int day;
    private String month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day >= 1 && day<= 31){
            this.day = day;
        }else{
            this.day = 1;
        }
    }

    @Override
    public String toString() {
        return "MyDate{" + "day=" + day + ", month=" + month + ", year=" + year + '}';
    }

    public MyDate(int day, String month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    
    
}
