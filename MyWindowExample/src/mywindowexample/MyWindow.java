
package mywindowexample;
import javax.swing.*;

public class MyWindow extends JFrame {
   private JLabel lblMessage;
   private JButton btnAction;
   MyWindow(){
       System.out.println("call MyWindow()");
       this.setSize(400, 350);
       this.setTitle("Первое окно");
       lblMessage = new JLabel("Привет мир!");
       btnAction = new JButton("click me");
        
       this.setLayout(new FlowLayout());
       
       this.add(lblMessage);

       this.add(btnAction);
       
       
       this.setVisible(true);
   } 
}
