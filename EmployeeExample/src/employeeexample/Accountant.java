
package employeeexample;


public class Accountant extends Cleaner implements IEmployee{
    private double coefficient;

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public Accountant(double salary, double cof) {
        super(salary);
        this.coefficient = cof;
    }

    @Override
    public double calcSalary() {
        return this.getA()*getCoefficient();
    }

    @Override
    public String getTitile() {
        return "Accountant";
    }
    
    
            
    
    
}
