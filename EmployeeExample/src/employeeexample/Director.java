
package employeeexample;


public class Director extends Accountant  implements IEmployee{
    private double bonus;

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public Director(double bon, double salary, double cof) {
        super(salary, cof);
        this.bonus = bon;
    }

    @Override
    public double calcSalary() {
        return getA()*getCoefficient()+getBonus();
    }

    @Override
    public String getTitile() {
        return "Director ";
    }
    
}
