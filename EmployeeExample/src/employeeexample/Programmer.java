/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeeexample;


public class Programmer implements IEmployee{
    private double rate;
    private int hour;

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public double getRate() {
        return rate;
    }

    public int getHour() {
        return hour;
    }
    Programmer(int Hour, double Salary){
        this.rate = Salary;
        this.hour = Hour;
    }

    @Override
    public double calcSalary() {
        return this.getHour()*getRate();
    }

    @Override
    public String getTitile() {
        return "Programmer";
    }

       
}
