
package employeeexample;


public class Cleaner implements IEmployee{
    private double base;
    Cleaner(double salary){
        this.base = salary;
    }

    public double getA(){return this.base;}
    public void setA(double base){this.base = base;}
    @Override
    public double calcSalary() {
        return this.base;
    }

    @Override
    public String getTitile() {
        return "Cleaner";
    }
}
