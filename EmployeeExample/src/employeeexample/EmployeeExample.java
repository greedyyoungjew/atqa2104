
package employeeexample;

import java.util.ArrayList;
import java.util.List;


public class EmployeeExample {

    
    public static void main(String[] args) {
        
        Cleaner cln = new Cleaner(6500);
        Accountant acc = new Accountant(6500, 4);
        Programmer dev = new Programmer(160, 280);
        Director direct = new Director(16000, 6500, 8);
        List<IEmployee> persons = new ArrayList<>();
        persons.add(cln);
        persons.add(acc);
        persons.add(dev);
        persons.add(direct);
        for(int i = 0; i<persons.size(); i++){
            double S = persons.get(i).calcSalary();
            String str = persons.get(i).getTitile();
            System.out.println(" Salary = "+S+" "
                    + "Person: "+str);
        }
    }
    
}
