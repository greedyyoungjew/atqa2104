
package javareflectiontest;

import java.io.Serializable;


public class User implements Serializable
{

    public Long getId() {
        System.out.println("вызван метод getId()");
        return id;
    }

    public void setId(Long id) {
        System.out.println("вызван метод setId()");
        this.id = id;
    }

    public String getEmail() {
        System.out.println("вызван метод getEmail()");
        return email;
    }

    public void setEmail(String email) {
        System.out.println("вызван метод setEmail() с параметром "+email);
        this.email = email;
    }

    public String getPassword() {
        System.out.println("вызван метод getPassword()");
        return password;
    }

    public void setPassword(String password) {
        System.out.println("вызван метод setPassword()");
        this.password = password;
    }

    public User(Long id) {
        System.out.println("вызван метод User(Long id)");
        this.id = id;
    }

    public User() {
        System.out.println("вызван метод User()");
    }

    public User(Long id, String email, String password) {
        System.out.println("вызван метод User(Long id, String email, String password)");
        this.id = id;
        this.email = email;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", email=" + email + ", password=" + password + '}';
    }
    public Long id;
    private String email;
    private String password;
}
    
    

