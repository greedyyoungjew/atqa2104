
package mathshapes;
import java.util.ArrayList;
import java.util.List;
public class MathShapes {

   
    public static void main(String[] args) {
        Square sq = new Square(3) {
            @Override
            public String getMyClassName() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        
        RecTangle rec = new RecTangle(3,4) {
            @Override
            public String getMyClassName() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        
        Triangle trg = new Triangle(3,4,5) {
            @Override
            public String getMyClassName() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        Circle cir = new Circle(3) {
            @Override
            public String getMyClassName() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        
        List<IMathShape> shapes = new ArrayList<>();
        shapes.add(sq);
        shapes.add(rec);
        shapes.add(trg);
        shapes.add(cir);
        
        for(int i = 0; i<shapes.size(); i++){
            double P = shapes.get(i).calcPerimetr();
            double S = shapes.get(i).calcArea();
            System.out.println(shapes.get(i).getClass());
            System.out.println(shapes.get(i).toString());
            System.out.println("s = "+S+" p = "+P);
        }
        
        
        
    }
    
}
