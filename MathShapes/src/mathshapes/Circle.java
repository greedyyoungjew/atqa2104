
package mathshapes;

public abstract class Circle extends Square implements IMathShape{
    
    public Circle(int a) {
        super(a);
    }
     public double calcArea(){
        return this.getA()*this.getA()*Math.PI;
    }
    public double calcPerimetr(){
        return this.getA() * 2*Math.PI;
    }
}
