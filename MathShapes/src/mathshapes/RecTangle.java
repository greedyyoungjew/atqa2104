
package mathshapes;

public abstract class RecTangle  extends Square implements IMathShape{
    
    private int b;    
    public RecTangle(int a, int b) {
        super(a);
        this.setB(b);
    }
    @Override
    public String toString() {
        return super.toString() + "b=" + this.getB();
    }
    public int getB() {
        return b;
    }
    public void setB(int b) {
        this.b = b;
    }
    
    @Override
    public double calcArea()
    {
        return this.getA()* this.getB();
    }
     @Override
    public double calcPerimetr()
    {
        return (this.getA() + this.getB()) * 2;
    }
    
}
      

