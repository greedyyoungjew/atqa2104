 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathshapes;

/**
 *
 * @author Zver
 */
public abstract class Square implements IMathShape{
    private int a;

    
    public Square(int a){
        this.a = a;
    }
    public int getA(){return this.a;}
public void setA(int a){this.a = a;}
    @Override
    public String toString() {
        return  "a=" + a;
    }
    public double calcArea(){
        return this.getA()*this.getA();
    }
    public double calcPerimetr(){
        return this.getA() * 4;
    }
}
