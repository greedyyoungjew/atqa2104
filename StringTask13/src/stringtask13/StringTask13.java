/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringtask13;
import java.util.Scanner;

/**
 *
 * @author Zver
 */
public class StringTask13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int N ; String str;
        Scanner input = new Scanner(System.in);
        N = input.nextInt();
        str = input.next();
        int counter = 0;
        for(int i =0; i<str.length();i++){
            counter++;
        }
        if(N>counter){
            int d = N - counter;
            for(int j = 1; j<=d;j++){
                System.out.print(".");
            }
            System.out.print(str);
        }else if(N<counter){
            int D = counter -N;
            StringBuffer stringBuffer = new StringBuffer(str);
            stringBuffer.delete(0, D);
            System.out.println(stringBuffer.toString());
            
        }
        
    }
    
}
