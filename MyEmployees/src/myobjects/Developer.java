/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myobjects;

/**
 *
 * @author Zver
 */
public class Developer implements IEmployee {
    private double rate;
    private double hours;
    private double salary;
    public Developer(){
        this.hours = 160;
        this.rate = 1000/this.hours;
        this.salary = 1000;
    }
    public Developer(double hh, double rate){
        this.hours = hh;
        this.rate = rate;
    }

    @Override
    public double calcSalary() {
        this .salary = this.hours*this.rate;
        return this.salary;
    }

    @Override
    public String getTitle() {
        return "Developer";
    }

    @Override
    public double calcTax() {
        return this.calcSalary()*0.065;
    }
    
    
}

    
