/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junitsquaretest;

/**
 *
 * @author Zver
 */
public class Square implements Interfase{
    private double a ;
    public Square(double a){
       this.a = a;

    }

    @Override
    public double calcArea() {
        return a*a;
    }

    @Override
    public double calcPerimetr() {
        return 4*a;
    }

    
}
