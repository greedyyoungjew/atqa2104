/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittest1;
import myobjects.*;

/**
 *
 * @author Zver
 */
public class TestCase3 extends TestCase  {
    IEmployee emp1;
     public TestCase3(){
        this.setUp();
    }
    protected void setUp(){
        this.emp1 = new Developer();
        
    }
    public String logResult(){
        
        if(this.test())
            return "test_calcTax_for_Developer_pass";
        else
        return "test_calcTax_for_Developer_fail";
    }
    protected boolean test(){
        double tax = this.emp1.calcTax();
        return tax == 65;
    }
}
