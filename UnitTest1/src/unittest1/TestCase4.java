/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittest1;
import myobjects.*;
/**
 *
 * @author Zver
 */
public class TestCase4 extends TestCase {
    IEmployee empl;
     public TestCase4(){
        this.setUp();
    }
    private void setUp(){
        this.empl = new Cleaner(1000);
    
        
    }
    public String logResult(){
        
        if(this.test())
            return "test_calcTax_for_Cleaner_pass";
        else
        return "test_calcTax_for_Cleaner_fail";
    }
    private boolean test(){
        double tax = this.empl.calcTax();
        return tax == 65;
    }
}
