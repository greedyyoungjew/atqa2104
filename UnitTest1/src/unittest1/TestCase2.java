/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittest1;
import myobjects.*;
/**
 *
 * @author Zver
 */
public class TestCase2 extends TestCase {
    IEmployee emp1;
     public TestCase2(){
        this.setUp();
    }
    protected void setUp(){
        this.emp1 = new Developer();
    }
    public String logResult(){
        
        if(this.test())
            return "test_calcSalary_for_Developer_pass";
        else
        return "test_calcSalary_for_Developer_fail";
    }
    private boolean test(){
        //double result = emp.calcSalary();
        double result = emp1.calcSalary();
        return result == 1000;
    }
}
