
package unittest1;
import myobjects.*;

public class TestCase5 extends TestCase{
    IEmployee emp1;
     public TestCase5(){
        this.setUp();
    }
    private void setUp(){
        this.emp1 = new Developer(160,16.5);
    }
    public String logResult(){
        
        if(this.test())
            return "test_calcSalary_for_Middle_Developer_pass";
        else
        return "test_calcSalary_for_Middle_Developer_fail";
    }
    private boolean test(){
        //double result = emp.calcSalary();
        double result = emp1.calcSalary();
        return result == 160*16.5;
    }
    
}
