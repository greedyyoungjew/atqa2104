
package txtfiletester;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;

import java.io.PrintWriter;

import java.io.IOException;
import java.io.InputStreamReader;

public class TxtFileTester {

    
    public static void main(String[] args) {
        System.out.println("Введите значение ");
        System.out.println("Stop");
        PrintWriter output = null;
        try {
            
            //создаем файл и пишем в него данные
            BufferedReader input = new BufferedReader(
                    new  InputStreamReader(System.in));
            output = new PrintWriter(
                    new FileWriter("data.txt"));
            while(true)
            {
                //считываем воод с клавиатуры и записываем, то что
                //прочли в файл
                String strtmp = input.readLine();
                if (strtmp.equals("stop"))//если ввели слово stop прекратили
                    break;
                output.println(strtmp);
            }
            output.close();
        } catch (IOException ex) {
            System.out.println("Не могу создать файл. Нет прав на запись.");
        }
        finally{
           output.close(); 
        }
        System.out.println("вывод файла data.txt");
        try {
            BufferedReader inputFile = new BufferedReader(new FileReader("input.txt"));
            String strtmp2;
            while ((strtmp2 = inputFile.readLine()) != null) {
                System.out.println(strtmp2);
                //double d = Double.parseDouble(strtmp2);
            }
            inputFile.close();
        } catch (Exception e) {
                        System.out.println("Не могу открыть файл. Нет прав или файл не существует.");

        }

    }
    
}
